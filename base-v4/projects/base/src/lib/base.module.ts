import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { NgCoreCoreModule } from '@ngcore/core';

// import { SampleComponent } from './src/sample.component';
// import { SampleDirective } from './src/sample.directive';
// import { SamplePipe } from './src/sample.pipe';
// import { SampleService } from './src/sample.service';

// // import { LocalUsers } from './src/data/core/local-users';
// import { AppUser } from './src/data/core/app-user';

// // import { PouchRepoDBNames } from './src/storage/pouch/pouch-repo-db-names';
// import { AppUserPouchRepo } from './src/storage/pouch/app-user-pouch-repo';

// import { AppUserHelper } from './src/providers/app-user-helper';

// import { BaseDatabaseService } from './src/services/base-database-service';



// // TBD:
// var dbNamePrefix = "ngcore-db1";
// // ...
// export function provideAppUserPouchRepo(): AppUserPouchRepo {
//   return new AppUserPouchRepo(dbNamePrefix + '-' + AppUser.name.toLowerCase());
//   // let repo = new AppUserPouchRepo();
//   // repo.dbName = dbNamePrefix + '-' + AppUser.name.toLowerCase();
//   // return repo;
// }


@NgModule({
  imports: [
    CommonModule,
    // BrowserModule,
    NgCoreCoreModule.forRoot()
  ],
  declarations: [
    // SampleComponent,
    // SampleDirective,
    // SamplePipe
  ],
  exports: [
    // SampleComponent,
    // SampleDirective,
    // SamplePipe
  ]
  // ,
  // providers: [
  //   { provide: AppUserPouchRepo, useFactory: provideAppUserPouchRepo }
  // ]

})
export class NgCoreBaseModule {
  static forRoot(): ModuleWithProviders<NgCoreBaseModule> {
    return {
      ngModule: NgCoreBaseModule,
      providers: [

        // { provide: AppUserPouchRepo, useFactory: provideAppUserPouchRepo },
        // AppUserHelper,

        // BaseDatabaseService

      ]
    };
  }
}
