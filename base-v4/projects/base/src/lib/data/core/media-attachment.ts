import { ColorTag, ColorTagUtil, DateTimeUtil, RandomIdUtil, UniqueIdUtil } from '@ngcore/core';
import { DataType } from './data-type';
import { MediaType } from './media-type';


/**
 * Represents an "attachment" to an object.
 */
export class MediaAttachment {

  // Note: We use name, not label, for sorting.
  public static compareAscending(a: MediaAttachment, b: MediaAttachment): number {
    // tbd: check name first before mediaType ????
    if (a.mediaType == b.mediaType) {
      if (a.name === b.name) {
        return a.createdTime - b.createdTime;
      } else {
        if (a.name == null) {
          return -1;
        } else if (b.name == null) {
          return 1;
        } else if (a.name < b.name) {
          return -1;
        } else if (a.name > b.name) {
          return 1;
        } else {
          return 0;
        }
      }
    } else {
      return a.mediaType - b.mediaType;
    }
  }
  public static compareDecending(a: MediaAttachment, b: MediaAttachment): number {
    if (a.mediaType == b.mediaType) {
      if (a.name === b.name) {
        return b.createdTime - a.createdTime;
      } else {
        if (a.name == null) {
          return 1;
        } else if (b.name == null) {
          return -1;
        } else if (a.name > b.name) {
          return -1;
        } else if (a.name < b.name) {
          return 1;
        } else {
          return 0;
        }
      }
    } else {
      return b.mediaType - a.mediaType;
    }
  }


  // false means it's just a local (temporary) object which has never been saved (or fetched from DB).
  get isSynched(): boolean {
    return (this.synchedTime > 0);
  }
  markAsSynched() {
    this.synchedTime = DateTimeUtil.getUnixEpochMillis();
    this.updatedTime = this.synchedTime;  // ????
  }

  get isDeleted(): boolean {
    return (this.deletedTime > 0);
  }
  markAsDeleted(deleted: boolean = true) {
    this.deletedTime = (deleted === true) ? DateTimeUtil.getUnixEpochMillis() : 0;
  }

  public isHidden: boolean = false;
  // getIsHidden(): boolean { return this.isHidden; }
  setIsHidden(_isHidden: boolean) { this.isHidden = _isHidden; this.isDirty = true; }

  // tags/labels
  // labels: string[] = [];  // ???
  public isFavorite: boolean = false;
  // getIsFavorite(): boolean { return this.isFavorite; }
  setIsFavorite(_isFavorite: boolean) { this.isFavorite = _isFavorite; this.isDirty = true; }
  // ...


  // If true, the data is updated but it has not been saved to DB since synchedTime.
  // When the data is saved it needs to be reset to false.
  isDirty: boolean = false;

  createdTime: number = 0;
  updatedTime: number = 0;
  // revisedTime: number = 0;   // Last updatd time before synch'ing to the server/db. This should be updated as a 'revision' number.
  synchedTime: number = 0;   // Last time it was feteched from DB, or saved to DB. (not the time synch'ed with the cloud data.)
  deletedTime: number = 0;   // Useful only if we use Trash Can.


  // TBD:
  // isPlayed?
  // playedTime ???


  // file name, media name, ...
  // Name is used for identifier, say, internally.
  name: (string | null) = null;
  setName(_name: string) { this.name = _name; this.isDirty = true; }

  // Label is used for display purposes.
  private _label: (string | null) = null;
  get label(): string {   // empty label is allowed.
    return (this._label != null) ? this._label : ((this.name != null) ? this.name : '');
  }
  set label(_label: string) {
    this._label = _label; 
    this.isDirty = true; 
  }


  // To support couchdb/pouchdb.
  // TBD: temporary implementation --> Need to revisit this some day....

  // TBD:
  // mediaType and contentType should be consistent. How to ensure that???

  mediaType: MediaType = MediaType.unknown;
  setMediaType(_mediaType: MediaType) { this.mediaType = _mediaType; this.isDirty = true; }

  contentType: (string | null) = null;
  setContentType(_contentType: string) { this.contentType = _contentType; this.isDirty = true; }

  dataType: DataType = DataType.unknown;
  setDataType(_dataType: DataType) { this.dataType = _dataType; this.isDirty = true; }

  contentData: any;    // Could be string, or byte array.
  setContentData(_contentData: any) { this.contentData = _contentData; this.isDirty = true; }


  get isStub(): boolean {
    return (this.dataType == DataType.stub);
  }
  get isRemote(): boolean {
    return (this.dataType == DataType.remote);
  }


  // digest?
  // Just use contentData as digest if dataType == stub
  get digest(): string {
    if (this.dataType == DataType.stub) {
      return this.contentData;
    } else {
      // return null;  // ???
      return '';    // ???
    }
  }
  set digest(_digest: string) {
    if (_digest) {
      this.contentData = _digest;
      this.dataType = DataType.stub;
    } else {
      // ignore????
    }
  }

  get data(): any {
    // if(this.dataType == DataType.binary || this.dataType == DataType.base64) {
    //   return this.contentData;
    if (this.dataType === DataType.base64) {
      return this.contentData as string;
    } else if (this.dataType == DataType.binary) {
      return this.contentData;
    } else {
      return null;
    }
  }
  set data(_data: any) {
    if (_data) {
      if ((typeof _data) == 'string') {
        this.contentData = _data as string;
        this.dataType = DataType.base64;
      } else {
        this.contentData = _data;
        this.dataType = DataType.binary;
      }
    } else {
      // ignore????
    }
  }

  // PouchDB still stores this as base64-encoded ????
  get url(): string {
    if (this.dataType == DataType.remote) {
      return this.contentData;
    } else {
      // return null;  // ???
      return '';    // ???
    }
  }
  set url(_url: string) {
    if (_url) {
      this.contentData = _url;
      this.dataType = DataType.remote;
    } else {
      // ignore????
    }
  }


  // id/userId (and some other fields) are not used when the attachment is saved as part of the doc.
  // These are relevant only when "remote" data type is used.
  // Note the order: object id - userId.
  // constructor(public id: string = null, public userId: string = null) {
  //   this.id = id;
  //   this.userId = userId;
  //   this.resetCreatedTime();
  // }
  // constructor(public id: (string | null) = null, public userId: (string | null) = null) {
  id: string;
  userId: string;
  constructor(id: (string | null) = null, userId: (string | null) = null) {
    if (id) {  // "0" is not a valid id. ????
      this.id = id;
    } else {
      this.id = UniqueIdUtil.id();
    }
    if (userId) {  // "0" is not a valid id. ????
      this.userId = userId;
    } else {
      this.userId = RandomIdUtil.id();
    }
    this.resetCreatedTime();
  }


  // TBD: Shouldn't createdTime be set when the object is saved????
  // protected resetCreatedTime(millis: number = 0) {
  protected resetCreatedTime(millis: number = DateTimeUtil.getUnixEpochMillis()) {
    this.createdTime = millis;
    this.updatedTime = this.createdTime;
    // this.revisedTime = this.createdTime;   // ??  Always start from non-0/non-null value.
    this.synchedTime = 0;
    this.deletedTime = 0;
    this.isDirty = true;
    // ...
  }
  protected resetUpdatedTime(millis: number = DateTimeUtil.getUnixEpochMillis()) {
    this.updatedTime = millis;
    if (this.createdTime === 0) {
      this.createdTime = this.updatedTime;
    }
    // // this.revisedTime = 0;   // ??
    // this.synchedTime = 0;
    // this.deletedTime = 0;
    this.isDirty = true;
  }

  toString(): string {
    return 'id:' + this.id
      + ';userId:' + this.userId
      + ';name:' + this.name
      + ';label:' + this.label
      + ';mediaType:' + this.mediaType
      + ';contentType:' + this.contentType
      + ';dataType:' + this.dataType
      // + ';contentData:' + this.contentData
      + ';isDirty:' + this.isDirty
      + ';isHidden = ' + this.isHidden
      + ';isFavorite = ' + this.isFavorite
      + ';createdTime:' + this.createdTime
      + ';updatedTime:' + this.updatedTime
      + ';synchedTime:' + this.synchedTime
      + ';deletedTime:' + this.deletedTime;
  }

  clone(): MediaAttachment {
    let cloned = Object.assign(new MediaAttachment(), this) as MediaAttachment;
    return cloned;
  }
  static clone(obj: any): MediaAttachment {
    let cloned = Object.assign(new MediaAttachment(), obj) as MediaAttachment;
    return cloned;
  }

  copy(): MediaAttachment {
    let obj = this.clone();
    obj.id = UniqueIdUtil.id();
    obj.resetCreatedTime();
    return obj;
  }

}
