

import { ColorTag, ColorTagUtil, DateTimeUtil, RandomIdUtil, UniqueIdUtil } from '@ngcore/core';


// Note that our "Model" plays dual roles, model and viewModel, in most cases.
export abstract class BaseModel {

  // Note: We can actually use different pouch doc.Id from model.id
  // Just overwrite this pair in a derived class.
  // getDocId(): string {
  //   return this._id;
  // }
  // setDocId(_id: string) {
  //   this._id = _id;
  // }

  // // Or, just use the class name as a prefix (so that they can be queried.)
  // getDocId(): string {
  //   return this.constructor.name.toLowerCase() + ':' + this._id;
  // }
  // setDocId(_id: string) {
  //   this._id = (_id && _id.length > this.constructor.name.length + 1)
  //     ? _id.substr(this.constructor.name.length + 1)
  //     : _id;  // ???
  // }

 
  // Important Note:
  // docId should be immutable.
  // Once an object is created, it should NOT change.
  // This rule should apply to all derived classes as well.........

  // Use userid:id combo (so that they can be queried based on the users.)
  // Requires different types of objects to be stored in different databases.
  // Note the order.
  static getDocId(userId: (string | null), id: (string | null)): string {
    let docId = '';
    if(userId != null) {
      docId += userId;
    }
    docId += ':';
    if(id != null) {
      docId += id;
    }
    return docId;
  }
  getDocId(): string {
    return this.userId + ':' + this.id;
  }
  setDocId(_id: string) {
    let idx = -1;
    if (_id) {
      idx = _id.indexOf(':');
    }
    if (idx === -1) {
      this.userId = '';  // ???
      this.id = _id;
    } else {
      this.userId = _id.substring(0, idx);
      this.id = (_id.length > idx + 1) ? _id.substring(idx + 1) : "";  // ???
    }
  }

  // Object version ???
  // version: number;

  // Every model object has a unique id associated with it.
  // id: string;     // --> Moved to ctor.
  // userId: string  // --> Moved to cotr.



  // Set it to true if it's saved on DB for the first time?
  // --> Just use synchedTime. If synchedTime == 0, it means it has never been stored.
  // isStored: boolean = false;

  // false means it's just a local (temporary) object which has never been saved (or fetched from DB).
  get isSynched(): boolean {
    return (this.synchedTime > 0);
  }
  markAsSynched() {
    this.synchedTime = DateTimeUtil.getUnixEpochMillis();
    this.updatedTime = this.synchedTime;  // ????
  }

  get isDeleted(): boolean {
    return (this.deletedTime > 0);
  }
  markAsDeleted(deleted: boolean = true) {
    this.deletedTime = (deleted === true) ? DateTimeUtil.getUnixEpochMillis() : 0;
  }


  // Note that these values can go out of sync between the client-side instances and those stored on DB.
  // ....

  // If true, the data is updated but it has not been saved to DB since synchedTime.
  // When the data is saved it needs to be reset to false.
  isDirty: boolean = true;   // Always start as dirty?

  createdTime: number = 0;
  updatedTime: number = 0;
  // revisedTime: number = 0;   // Last updatd time before synch'ing to the server/db. This should be updated as a 'revision' number.
  synchedTime: number = 0;   // Last time it was feteched from DB, or saved to DB. (not the time synch'ed with the cloud data.)
  deletedTime: number = 0;   // Useful only if we use Trash Can.


  // // "Table". kind or type?
  // // private _kind: string;
  // get kind(): string {
  //   // return this._kind;

  //   // Does this work in the derived classes???
  //   return this.constructor.name;  // ??
  // }
  // // set kind(_kind: string) {
  // //   this._kind = _kind;
  // // }

  // // // Or, this?
  // // getKind(): string {
  // //   // Does this work in the derived classes???
  // //   return this.constructor.name;  // ??
  // // }





  // // In order to support PouchDB, we use _id field.
  // // Internally in the app, we use id.
  // get id(): string {
  //   return this._id;
  // }
  // set id(_id: string) {
  //   // validate id?
  //   this._id = _id;
  // }

  // // To support couchdb/pouchdb.
  // private _rev: string = null;
  // get rev(): string {
  //   return this._rev;
  // }
  // set rev(_rev: string) {
  //   this._rev = _rev;
  // }

  // // All objects require userId. (owner id? or creator id?).
  // // Can we change the ownership of the object? Or, is userId referring to the user who created it?
  // // --> Owner makes more sense.
  // get userId(): string {
  //   // Does it make sense to automatically set userId ???
  //   if (!this._userId) {
  //     this._userId = UniqueIdUtil.id();   // ???
  //   }
  //   return this._userId;
  // }
  // set userId(_userId: string) {
  //   // validate userid ??? 
  //   this._userId = _userId;
  //   this.isDirty = true;
  // }

  // // Currently, all ids are 12 characters long.
  // // Note the order: object id - userId.
  // constructor(private _id: string = null, private _userId: string = null) {
  //   if (_id) {  // "0" is not a valid id. ????
  //     this._id = _id;
  //   } else {
  //     // this.id = RandomIdUtil.id();
  //     this._id = UniqueIdUtil.id();
  //   }
  //   this.resetCreatedTime();

  //   // if(isDL()) dl.log(">>> id = " + this.id);
  // }



  // To support couchdb/pouchdb.
  // Note that because of the way we store rev (instead of PouchDoc._rev),
  // this can go out out sync with PouchDoc._rev (which is the real value as far as PouchDB is concerned....)
  //  --> What to do ????
  rev: (string | null) = null;

  // Currently, all ids are 12 characters long.
  // Note the order: object id - userId.
  // constructor(public id: (string | null) = null, public userId: (string | null) = null) {
  id: string;
  userId: string;
  constructor(id: (string | null) = null, userId: (string | null) = null) {
    if (id) {  // "0" is not a valid id. ????
      this.id = id;
    } else {
      this.id = UniqueIdUtil.id();
    }
    if (userId) {  // "0" is not a valid id. ????
      this.userId = userId;
    } else {
      // Note: We use RandomId instead of UniqueId for userId.
      //       This is necessary because userId can be used as Hash Key in DynamoDB.
      this.userId = RandomIdUtil.id();   // Does this make sense???
    }

    this.resetCreatedTime();

    // if(isDL()) dl.log(">>> id = " + this.id);
  }




  // TBD: Shouldn't createdTime be set when the object is saved????
  // protected resetCreatedTime(millis: number = 0) {
  protected resetCreatedTime(millis: number = DateTimeUtil.getUnixEpochMillis()) {
    this.createdTime = millis;
    this.updatedTime = this.createdTime;
    // this.revisedTime = this.createdTime;   // ??  Always start from non-0/non-null value.
    this.synchedTime = 0;
    this.deletedTime = 0;
    this.isDirty = true;
    this.rev = null;      // resetCreatedTime() when a new object is created. Hence, it's safe/requird to reset _rev.
  }
  protected resetUpdatedTime(millis: number = DateTimeUtil.getUnixEpochMillis()) {
    this.updatedTime = millis;
    if (this.createdTime === 0) {
      this.createdTime = this.updatedTime;
    }
    // // this.revisedTime = 0;   // ??
    // this.synchedTime = 0;
    // this.deletedTime = 0;
    this.isDirty = true;
  }

  toString(): string {
    return 'id:' + this.id
      + ';userId:' + this.userId
      + ';rev:' + this.rev
      + ';kind:' + this.constructor.name
      + ';isDirty:' + this.isDirty
      + ';createdTime:' + this.createdTime
      + ';updatedTime:' + this.updatedTime
      + ';synchedTime:' + this.synchedTime
      + ';deletedTime:' + this.deletedTime;
  }


  abstract clone(): BaseModel;
  abstract copy(): BaseModel;

  // clone(): BaseModel {
  //   // return JSON.parse(JSON.stringify(this)) as BaseModel;
  //   return Object.assign(new BaseModel(), this);
  // }
  // copy(): BaseModel {
  //   let obj = this.clone();
  //   obj.id = RandomIdUtil.id();
  //   obj.resetCreatedTime();
  //   return obj;
  // }
}
