import { async, inject, TestBed } from '@angular/core/testing';
import {} from 'jasmine';

import { AppUser } from './app-user';


// https://angular.io/docs/ts/latest/guide/testing.html

describe('AppUser class', () => {
  let userId = "100";
  let appUser: AppUser;

  beforeEach(async(() => {
    appUser = new AppUser(userId);
  }));

  it ('should have correct default user id', () => {
    // expect(AppUser.defaultUserId).toBe("1");
    expect(AppUser.defaultUserId).not.toBeNull();
  });

  it ('should have correct instance user id', () => {
    expect(appUser.id).toBe(userId);
  });

});
