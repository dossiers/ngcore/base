import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { ColorTag, ColorTagUtil, RandomIdUtil } from '@ngcore/core';

import { LocalUsers } from './local-users';
import { BaseModel } from './base-model';


/**
 * Represents a Website visitor/app user.
 */
// Note:
// AppUser vs UserPreference.
// AppUser is for mostly semi-permanent readonly data.
// User preference is for user settings (that can be easily changed by the user)
export class AppUser extends BaseModel {

  // AppUser is special in that it uses the same value for id and userId. appUser.id == userId.

  // Just use userId as the doc id.
  static getDocId(userId: (string | null)): string {
    return (userId) ? userId : '';  // ???
  }
  getDocId(): string {
    // return (this.userId != null) ? this.userId : '';   // ???
    return this.userId;
  }
  setDocId(_id: string) {
    this.userId = _id;
  }


  // temporary
  // default user id.
  private static _defaultUserId: string = LocalUsers.DEFAULT_USER_ID;
  // Error: Metadata collected contains an error that will be reported at runtime
  // static get defaultUserId(): string {
  //   return AppUser._defaultUserId;
  // }
  // static set defaultUserId(_defaultUserId: string) {
  //   AppUser._defaultUserId = _defaultUserId;
  // }
  static getDefaultUserId(): string {
    return AppUser._defaultUserId;
  }
  static setDefaultUserId(_defaultUserId: string) {
    AppUser._defaultUserId = _defaultUserId;
  }


  // id: string = '0';  // user id from the server....
  // tbd: colorTag/avatar should really be associated with UserAccount.
  colorTag: ColorTag;   // Use egg-shape/circular images ????
  // --> Use avatar image? (Or, use the tag as default and override with photo/image if uploaded???)
  //    (To be done in UserProfile?)


  // User session tracking

  public lastSessionTime: number = 0;
  // getLastSessionTime(): number { return this.lastSessionTime; }
  setLastSessionTime(_lastSessionTime: number) { this.lastSessionTime = _lastSessionTime; this.isDirty = true; }

  // Does this make sense?
  // In general, if the appUser exists in the DB, it's very likely the user has logged on before....
  get isFirstSession(): boolean {
    return (this.lastSessionTime === 0);
  }



  public primaryEmail: (string | null) = null;
  // getPrimaryEmail(): string { return this.primaryEmail; }
  setPrimaryEmail(_primaryEmail: string) { this.primaryEmail = _primaryEmail; this.isDirty = true; }

  public primaryPhone: (string | null) = null;
  // getPrimaryPhone(): string { return this.primaryPhone; }
  setPrimaryPhone(_primaryPhone: string) { this.primaryPhone = _primaryPhone; this.isDirty = true; }


  //fullname: string;

  public firstName: (string | null) = null;
  // getFirstName(): string { return this.firstName; }
  setFirstName(_firstName: string) { this.firstName = _firstName; this.isDirty = true; }

  public lastName: (string | null) = null;
  // getLastName(): string { return this.lastName; }
  setLastName(_lastName: string) { this.lastName = _lastName; this.isDirty = true; }

  // // TBD: Move this to UserProfile?
  // private _nickname: string = null;
  // get nickname(): string { return this._nickname; }
  // set nickname(_nickname: string) { this._nickname = _nickname; this.isDirty = true; }


  public isLocal: boolean = false;
  // getIsLocal(): boolean { return this.isLocal; }
  setIsLocal(_isLocal: boolean) { this.isLocal = _isLocal; this.isDirty = true; }

  public isHidden: boolean = false;
  // getIsHidden(): boolean { return this.isHidden; }
  setIsHidden(_isHidden: boolean) { this.isHidden = _isHidden; this.isDirty = true; }


  avatarImage: string;   // URL
  headerImage: string;   // URL
  backgroundColor: string;


  // override ???
  // get userId(): string {
  //   if(!super._userId) {
  //     // Does this make sense???
  //     super._userId = UniqueIdUtil.id();   // ???
  //   }
  //   return super._userId;
  // }
  // set userId(_userId: string) {
  //   this._userId = _userId; this.isDirty = true;
  // }

  // getUsername(): string { return this.username; }
  setUsername(_username: string) { this.username = _username; this.isDirty = true; }

  // TBD: Now we end up with two ids. BaseModel.id and AppUser._id...
  //      What to do ????
  // Note: For AppUser, .id and .userId are the same.
  //      (TBD: For now, we need to set the default value for _uid here. Otherwise, they will be set with different values in BaseMode.)
  // TBD: How to enforce this constraint so that setters for .id and .user)d alwways end up with the same value???? 
  constructor(private _uid: string = RandomIdUtil.id(), public username: (string | null) = null) {
    // Note: AppUser is the only model that uses RandomId instead of UniqueId.
    //       This is necessary because userId can be used as Hash Key in DynamoDB.
    super(_uid, _uid);  // id == userId.
  }

  // temporary
  setId(_id: string) {
    this.id = _id;
    this.userId = _id;
    this.isDirty = true;
  }
  setUserId(_uid: string) {
    this.id = _uid;
    this.userId = _uid;
    this.isDirty = true;
  }
  // temporary


  toString(): string {
    return super.toString()
      + '; username = ' + this.username
      + '; colorTag = ' + this.colorTag
      + '; primaryEmail = ' + this.primaryEmail
      + '; primaryPhone = ' + this.primaryPhone;
  }

  clone(): AppUser {
    let cloned = Object.assign(new AppUser(), this) as AppUser;
    return cloned;
  }
  static clone(obj: any): AppUser {
    let cloned = Object.assign(new AppUser(), obj) as AppUser;
    return cloned;
  }

  copy(): AppUser {
    let obj = this.clone();
    // Note: Copying AppUser does not really make sense...
    // Use random id.
    // obj.id = RandomIdUtil.id();
    obj.setId(RandomIdUtil.id());   // We have to change both id and userId.
    // ...
    obj.resetCreatedTime();
    return obj;
  }
}
