/**
 * Attachment data type.
 */
export enum DataType {
  unknown = 0,
  binary = 1,       // blob. byte array
  base64 = 2,       // base64 encoded.
  // utf16 = 4,        // binary converted to UTF16 character
  stub = 8,         // No data.
  remote = 16,      // media URL
}
