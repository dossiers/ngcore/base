# @ngcore/base
> NG Core angular/typescript base library


Base classes library for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/base/




